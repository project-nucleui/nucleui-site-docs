[Code](/code/) > [PageStructure.jsx](/code/page-structure)

# src/PageStructure.jsx

<!-- notices -->
> ## DEPRECATION NOTICE
>
> `PageStructure` is planned to be deprecated after the introduction of [`NeueUI.Types.Document`](/code/document), which will handle populating the [document](/code/document) root and registering the new [`Document` component](/code/document).
<!-- endnotices -->

<!-- articlecontent -->

The `PageStructure` component defines the overall structure of your documents, defining the `html`, `head`, `body`, and `reactroot` components.

## Example

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

export default (NeueUI) => NeueUI.register('views$base:PageStructure', {
	component: class PageStructure extends React.Component {
		render(){
			return (
				<html>
					<head>
						<title>NeueLogic</title>
						<link rel="stylesheet" href="/css/reset.css" />
						<link rel="stylesheet" href="/css/global.css" />
					</head>
					<body>
						<main dangerouslySetInnerHTML={{__html:this.props.children}} />
						<script src="/app.js"></script>
					</body>
				</html>
			);
		}
	}
});
```

<!-- endarticlecontent -->

<!-- seealso -->
##### Reference

* [**PageStructure**](/code/page-structure)

**Related:**
* [**NotFound Route**](/code/not-found)
* [**Styling Tips**](/code/styling-tips)
* [**ResourceRef**](/code/nui/resource-ref)
* [**Router**](/code/nui/router)
* [**Types**](/code/nui/types)

<!-- endseealso -->

<!-- topics -->
* [Quick Start Guide](/learn/quick-start)
* [Project Structure](/learn/project-structure)
* [Views, Stores, Actions: What Are They?](/learn/resources/what-are-they)
* [How Dependencies Work in Nui](/learn/how-dependencies-work)
<!-- endtopics -->
