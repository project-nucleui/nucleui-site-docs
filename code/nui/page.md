[Learn](/learn/) > Nui

# Nui.Types.Component

<!-- articlecontent -->
**`Components`** are the `view` portion of this framework, and inherit functionality from `React`'s `Component` prototype.

## Syntax

```javascript
export const Routes = ''; /* (pages only) */
export default (NeueUI) => class ComponentName extends NeueUI.Types.Component {
	render()
}
```

### Exports

##### `default`
>> Function which defines and returns the component class.
>>
>> Parameter ***`NeueUI`*** is the component's [NeueUI Interface](#neueui-interface)
##### `Routes` *`page`*
>> Defines a page component's routable path.
>>
>> See *`react-router`* documentation for [Routes Matching](https://github.com/ReactTraining/react-router/blob/master/docs/guides/RoutesMatching.md).

## Class Specification

The methods below can be defined to take advantage of various aspects of the framework.

### Properties

###### [`.state`](/code/nui/types/component/render)
> *`object`* Contains the component's state

###### [`.propTypes`](/code/nui/types/component/render)
> *`static`* *`object`* Defines the required types that each property should be.

###### [`.defaultProps`](/code/nui/types/component/render)
> *`static`* *`object`* Defines the default property values. Used when no props are passed in by the parent component.

### Methods

###### [`.render()`](/code/nui/types/component/render) *`nui`*
> Returns the specified action creator.


###### [`.renderWaiting()`](/code/nui/types/component/renderWaiting) *`nui`*
> Returns the specified action creator.


###### [`.componentWillMount()`](/code/nui/types/component/react/componentWillMount) *`react`*
> Returns the specified action creator.


###### [`.componentDidMount()`](/code/nui/types/component/react/componentDidMount) *`react`*
> Returns the specified action creator.


###### [`.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps) *`react`*
> Returns the specified action creator.


###### [`.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate) *`react`*
> Returns the specified action creator.


###### [`.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate) *`react`*
> Returns the specified action creator.


###### [`.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate) *`react`*
> Returns the specified action creator.


###### [`.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount) *`react`*
> Returns the specified action creator.

## Instance Members

Instance Members are methods and properties that are defined by the `type`'s Initializer during importation. These properties and methods are accessible on the instance, but should not be defined manually.

### Properties
###### [`this.ref`](/code/nui/ResourceRef)
> *`ResourceRef`* The fully qualified reference name for the component.

###### [`this.nui_Ready`](/code/nui/types/component/lifecycle)
> *`boolean`* Defines whether or not the component is ready to render.

###### [`this.nui_depsReady`](/code/nui/types/component/lifecycle)
> *`boolean`* Defines whether or not all the component's dependencies have fulfilled their `ready` state.

### Methods

###### [`this.Actions()`](/code/nui/types/component/view/Actions)
> Returns the specified action creator.

###### [`this.Stores()`](/code/nui/types/component/view/Stores)
> Returns the interface for the specified Store.

###### [`this.Views()`](/code/nui/types/component/view/Views)
> Returns the specified view component.


## NeueUI Interface

`NeueUI Interface` describes the aspects of the framework instance available for access within the component.

###### Core Objects
> [**`NeueUI.Config`**](/code/nui.html#config)

> [**`NeueUI.Logger`**](/code/pkgs/logger)

> [**`NeueUI.Routesr`**](/code/nui/router)

###### Types
> [**`NeueUI.Types.Component`**](/code/nui/types/component)

###### Resource Getters
> [**`NeueUI.Actions()`**](/code/nui.html#get)

> [**`NeueUI.Stores()`**](/code/nui.html#get)

> [**`NeueUI.Views()`**](/code/nui.html#get)

> [**`NeueUI.get()`**](/code/nui.html#get)

###### Methods
> [**`NeueUI.register()`**](/code/nui.html#register)


## Example Component

```javascript
import React from 'react';

export const Routes = '/';

export default (NeueUI) => class HomePage extends NeueUI.Types.Component {
	render() {
		const Header = this.Views('global/header');
		const Footer = this.Views('global/footer');
		return (
			<section>
				<Header />
				<p>Hello world!</p>
				<Footer />
			</section>
		);
	}
}
```
<!-- endarticlecontent -->

<!-- seealso -->
##### See Also

* [**NeueUI.Types.Component**](/code/nui/types/component)
	* [`Static.propTypes`](/code/nui/types/component/render)
	* [`Static.defaultProps`](/code/nui/types/component/render)
	* [`Instance.ref`](/code/nui/ResourceRef)
	* [`Instance.Actions()`](/code/nui/types/component/view/Actions)
	* [`Instance.Stores()`](/code/nui/types/component/view/Stores)
	* [`Instance.Views()`](/code/nui/types/component/view/Views)
	* [`Prototype.state`](/code/nui/types/component/render)
	* [`Prototype.render()`](/code/nui/types/component/render)
	* [`Prototype.renderWaiting()`](/code/nui/types/component/renderWaiting)

**Inheritance:**
* [**React.Component**](/code/nui/types/component)
	* [`Prototype.componentWillMount()`](/code/nui/types/component/react/componentWillMount)
	* [`Prototype.componentDidMount()`](/code/nui/types/component/react/componentDidMount)
	* [`Prototype.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps)
	* [`Prototype.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate)
	* [`Prototype.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate)
	* [`Prototype.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate)
	* [`Prototype.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount)

<!-- endseealso -->

<!-- topics -->
* [Creating Your First Component](/code/nui/types/component)
* [Creating Your First Page](/code/nui/types/component)
* [How Dependencies Work](/code/nui/types/component)
* [Todo App Example](/code/nui/types/component)
<!-- endtopics -->
