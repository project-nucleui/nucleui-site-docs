[Learn](/learn/) > Nui

# `Nui` Component

See also [*`Pages`*](/code/nui/page)

For stateful or Smart components, see [*`Nui.Types.Component`*](/code/nui/types/component)

For stateless or Dumb components, see [*`Component Function`*](/code/nui/view-function)

<!-- articlecontent -->
A `Nui` **`Component`** can be expressed either in [`Class`-like form](/code/nui/types/component), or as [a simple `Function`](/code/nui/view-function).

## Syntax

Both component types follow the standard `nui` *`export`* pattern. The component is always expected to be wrapped in a `nui` function in the `default export`.

The `nui` function is given a single parameter, the *`Nui Interface`* (see below), when the component is initially loaded into the application. This is leveraged for accessing various aspects of `Nui`, including *`Nui.Types.Component`* which is extended for stateful components.

### Stateful Components
```javascript
export default Nui => class ComponentName extends Nui.Types.Component {
	render()
}
```
See [*`Nui.Types.Component`*](/code/nui/types/component) class.

### Stateless Components
```javascript
export default Nui => (props) => { }
```
See [*`Component Function`*](/code/nui/view-function) pattern.

## Exports

##### `default` *`view`*
>> *`function`* defines and returns the component
>>
>> *`param`* *`object`* `nui` (See Nui Interface below)


## Example Component

```javascript
import React from 'react';

export default Nui => class Header extends Nui.Types.Component {
	render() {
		const Logo = this.Views('global/logo');
		const Nav = this.Views('global/nav');
		return (
			<header>
				<Logo />
				<Nav />
			</header>
		);
	}
}
```


## Nui Interface

`Nui Interface` describes the aspects of the framework instance available for access within the component.

###### Core Objects
> [**`Nui.Config`**](/code/nui.html#config)

> [**`Nui.Logger`**](/code/pkgs/logger)

> [**`Nui.Routesr`**](/code/nui/router)

###### Types
> [**`Nui.Types.Component`**](/code/nui/types/component)

###### Resource Getters
> *For stateful components use instance methods instead, like `this.Actions()`.*

> [**`Nui.Actions()`**](/code/nui.html#get)

> [**`Nui.Stores()`**](/code/nui.html#get)

> [**`Nui.Views()`**](/code/nui.html#get)

> [**`Nui.get()`**](/code/nui.html#get)

###### Methods
> [**`Nui.register()`**](/code/nui.html#register)

## Resource Identifier

*See also [Resource Identifiers](/learn/resources/what-are-they) and [How Dependencies Work](/code/nui/types/component)*

Views are identified using the following syntax:

```
views$module:path
```

**Parts**
* `views`
	* Always "views"
* `module`
	* Identifies the what module the view belongs to
	* Defaults to `base` (the current project)
* `path`
	* File path relative to the `module`'s *`views/`* directory, excluding file extension
	* Example: `todoApp/src/views/global/header.jsx` -> `global/header`

<!-- endarticlecontent -->

<!-- seealso -->
##### See Also

* [**Nui.Types.Component**](/code/nui/types/component)
	* [`Static.propTypes`](/code/nui/types/component/render)
	* [`Static.defaultProps`](/code/nui/types/component/render)
	* [`Instance.ref`](/code/nui/ResourceRef)
	* [`Instance.Actions()`](/code/nui/types/component/view/Actions)
	* [`Instance.Stores()`](/code/nui/types/component/view/Stores)
	* [`Instance.Views()`](/code/nui/types/component/view/Views)
	* [`Prototype.state`](/code/nui/types/component/render)
	* [`Prototype.render()`](/code/nui/types/component/render)
	* [`Prototype.renderWaiting()`](/code/nui/types/component/renderWaiting)

**Inheritance:**
* [**React.Component**](/code/nui/types/component)
	* [`Prototype.componentWillMount()`](/code/nui/types/component/react/componentWillMount)
	* [`Prototype.componentDidMount()`](/code/nui/types/component/react/componentDidMount)
	* [`Prototype.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps)
	* [`Prototype.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate)
	* [`Prototype.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate)
	* [`Prototype.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate)
	* [`Prototype.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount)

<!-- endseealso -->

<!-- topics -->
* [Creating Your First Component](/code/nui/types/component)
* [Creating Your First Page](/code/nui/types/component)
* [How Dependencies Work](/code/nui/types/component)
* [Smart vs Dumb Components](/code/nui/types/component)
* [Todo App Example](/code/nui/types/component)
<!-- endtopics -->
