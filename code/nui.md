[Code](/code/) > [Nui](/code/nui)

# Nui API
*For the core global object, see [`NeueUI`](/code/pkgs/nui/neueui) of the [`core`](/code/pkgs/nui) package.*

<!-- articlecontent -->
**`Nui`** refers to the API for a given [resource](/learn/resources/what-are-they) (be it a [`view`](/core/nui/view), [`store`](/code/nui/store), [`action`](/code/nui/action), or [`page`](/code/nui/page). The methods and objects available are based on the resource type.

## Syntax

This API object is passed in as the sole parameter to the *`default`* export of a [resource](/learn/resources/what-are-they).

```javascript
export default Nui => /* Resource Goes Here */
```

### Interfaces

See below for type-specific APIs

* [Actions API **`action`**](/code/nui/interfaces/action)
* [Store API **`store`**](/code/nui/interfaces/store)
* [Component API **`view` `page`**](/code/nui/interfaces/component)


<!-- endarticlecontent -->

<!-- seealso -->
##### See Also

* [**NeueUI.Types.Component**](/code/nui/types/component)
	* [`Static.propTypes`](/code/nui/types/component/render)
	* [`Static.defaultProps`](/code/nui/types/component/render)
	* [`Instance.ref`](/code/nui/ResourceRef)
	* [`Instance.Actions()`](/code/nui/types/component/view/Actions)
	* [`Instance.Stores()`](/code/nui/types/component/view/Stores)
	* [`Instance.Views()`](/code/nui/types/component/view/Views)
	* [`Prototype.state`](/code/nui/types/component/render)
	* [`Prototype.render()`](/code/nui/types/component/render)
	* [`Prototype.renderWaiting()`](/code/nui/types/component/renderWaiting)

**Inheritance:**
* [**React.Component**](/code/nui/types/component)
	* [`Prototype.componentWillMount()`](/code/nui/types/component/react/componentWillMount)
	* [`Prototype.componentDidMount()`](/code/nui/types/component/react/componentDidMount)
	* [`Prototype.componentWillReceiveProps()`](/code/nui/types/component/react/componentWillReceiveProps)
	* [`Prototype.shouldComponentUpdate()`](/code/nui/types/component/react/shouldComponentUpdate)
	* [`Prototype.componentWillUpdate()`](/code/nui/types/component/react/componentWillUpdate)
	* [`Prototype.componentDidUpdate()`](/code/nui/types/component/react/componentDidUpdate)
	* [`Prototype.componentWillUnmount()`](/code/nui/types/component/react/componentWillUnmount)

<!-- endseealso -->

<!-- topics -->
* [Creating Your First Component](/code/nui/types/component)
* [Creating Your First Page](/code/nui/types/component)
* [How Dependencies Work](/code/nui/types/component)
* [Todo App Example](/code/nui/types/component)
<!-- endtopics -->
