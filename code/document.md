[Code](/code/) > [document.jsx](/code/document)

# Document Component

<!-- articlecontent -->

The `Document` component defines the `html`, `head`, `body`, and `reactroot` structure for your site. This component is automatically rendered by Nui when serving each page.

While you it is currently possible to import other dependencies into the `Document` component, it is not recommended. *Due to the lifecycle of this component, it will only render on the server-side when the user first initially lands on the page.* With proper routing set up, the user will only retrieve the document element and trigger server-side rendering when the user initially visits your site.

## Syntax

*See also [Component Syntax](/code/nui/view#syntax).*

```javascript
import React from 'react';

export default (NeueUI) => ({ App, AppScript }) => (
	<html>
		<head>
			<title>Foobar</title>
		</head>
		<body>
			<App />
			<AppScript />
		</body>
	</html>
);
```

## Component Props

###### `App`
> *`component`* The reactroot component.

###### `AppScript`
> *`component`* The nui script component.

## Examples

### Adding Stylesheets
Stylesheets can be added the same way as any traditional HTML element. Simply add your *`<link />`* tag in the *`<head />`* and you're golden.

```javascript
import React from 'react';

export default (NeueUI) => ({ App, AppScript }) => (
	<html>
		<head>
			<title>NeueLogic</title>
			<link rel="stylesheet" href="/css/reset.css" />
			<link rel="stylesheet" href="/css/global.css" />
		</head>
		<body>
			<App />
			<AppScript />
		</body>
	</html>
);
```

### Example Using `styled-components`
A highly recommended and versatile way of styling is to use [`styled-components`](https://www.styled-components.com/), and rendering your stylesheets is incredibly easy by using the [*`ServerStyleSheet`*](https://www.styled-components.com/docs/advanced#server-side-rendering) export provided by [`styled-components`](https://www.styled-components.com/).

```javascript
import React from 'react';
import { ServerStyleSheet } from 'styled-components'

export default (NeueUI) => ({ App, AppScript }) => {
	const sheet = new ServerStyleSheet();
	const main = sheet.collectStyles(<App />);
	const styleTags = sheet.getStyleElement();

	return (
		<html>
			<head>
				<title>NeueLogic</title>
				{styleTags}
			</head>
			<body>
				{main}
				<AppScript />
			</body>
		</html>
	);
}
```

<!-- endarticlecontent -->

<!-- seealso -->
##### Document

* [**Syntax**](/code/document#syntax)
* [**Component Props**](/code/document#component-props)
	* [**props.App**](/code/document#app)
	* [**props.AppScript**](/code/document#appscript)
* [**Examples**](/code/document#examples)
	* [**Adding Stylesheets**](/code/document#adding-stylesheets)
	* [**Example Using `styled-components`**](/code/document#example-using-styled-components)

**Related:**
* [**NotFound Route**](/code/not-found)
* [**Styling Tips**](/code/styling-tips)
* [**ResourceRef**](/code/nui/resource-ref)
* [**Router**](/code/nui/router)
* [**Types**](/code/nui/types)

<!-- endseealso -->

<!-- topics -->
* [Quick Start Guide](/learn/quick-start)
* [Project Structure](/learn/project-structure)
* [Views, Stores, Actions: What Are They?](/learn/resources/what-are-they)
* [How Dependencies Work in Nui](/learn/how-dependencies-work)
<!-- endtopics -->
